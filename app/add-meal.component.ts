import { Component, EventEmitter } from 'angular2/core';
import { Meal } from './meal.model';

@Component({
  selector: 'add-meal',
  outputs: ['onAddMeal'],
  template:`
    <button class="btn btn-primary" (click)="showAddMealForm()" [class.hidden]="buttonHidden">Add a Meal</button>
    <div [class.hidden]="formHidden">
      <input placeholder="Name" #name>
      <input placeholder="Description" #description>
      <input placeholder="Calories" type="number" #calories>
      <button (click)="addMeal(name, description, calories)" class="btn btn-success">Add Meal</button>
    </div>

  `
})
export class AddMealComponent {
  public onAddMeal: EventEmitter<Meal>;
  formHidden: boolean = true;
  buttonHidden: boolean = false;
  constructor() {
    this.onAddMeal = new EventEmitter();
  }

  showAddMealForm() {
    this.formHidden = false;
    this.buttonHidden = true;
  }

  addMeal(name: HTMLInputElement, description: HTMLInputElement, calories: HTMLInputElement) {
    this.formHidden = true;
    this.buttonHidden = false;
    var newMeal = new Meal(name.value, description.value, Number(calories.value));
    this.onAddMeal.emit(newMeal);
    name.value = "";
    description.value = "";
    calories.value = "";
  }

}
