import {Pipe, PipeTransform} from 'angular2/core';
import {Meal} from './meal.model';

@Pipe ({
  name: "calorieFilter",
  pure: false
})

export class FilterPipe implements PipeTransform {
  transform(input: Meal[], args) {
    var filterState: number = args[0];
    if (filterState == 1) {
      return input.filter(function(meal) {
        return meal.calories < 500
      });
    } else if (filterState == 2) {
      return input.filter(function(meal) {
        return meal.calories >= 500
      });
    } else {
      return input;
    }
  }
}
