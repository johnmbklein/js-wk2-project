import {Component} from 'angular2/core';
import {Meal} from './meal.model';

@Component({
  selector: 'meal',
  inputs: ['meal'],
  template: `
  <h4>{{ meal.name }}</h4>
  <p>{{ meal.description }}</p>
  <p>{{ meal.calories }} Calories</p>
  `
})

export class MealComponent {
}
