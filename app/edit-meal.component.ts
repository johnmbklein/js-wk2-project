import {Component} from 'angular2/core';
import {Meal} from './meal.model';
import {MealListComponent} from './meal-list.component';

@Component({
  selector: 'edit-meal',
  inputs: ['selectedMeal'],
  template:   `
    <h5>Edit {{ selectedMeal.name }}<h5>
    <input placeholder="Name" [(ngModel)]="selectedMeal.name">
    <input placeholder="Description" [(ngModel)]="selectedMeal.description">
    <input placeholder="Calories" [(ngModel)]="selectedMeal.calories">
    <button (click)="mealUpdated()" class="btn btn-success">Update</button>
  `
})

export class EditMealComponent {
  public meal: Meal;
  editorHidden: boolean;

  constructor() {
  }

  mealUpdated(updatedMeal: Meal){
    console.log("click");
    this.editorHidden = true;
  }
}
