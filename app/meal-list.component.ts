import { Component, EventEmitter } from 'angular2/core';
import { MealComponent } from './meal.component';
import { AddMealComponent } from './add-meal.component';
import { EditMealComponent } from './edit-meal.component';
import { Meal } from './meal.model';
import { FilterPipe } from './filter.pipe';

@Component({
  selector: 'meal-list',
  inputs: ['meals'],
  outputs: ['onMealSelect'],
  pipes: [FilterPipe],
  directives: [MealComponent, AddMealComponent, EditMealComponent],
  template:`
    <add-meal (onAddMeal)="createMeal($event)"></add-meal>
    <h3>Today's Meals:</h3>
    <br>
    <select (change)="onChange($event.target.value)">
      <option value="0" selected="selected">Show All</option>
      <option value="1">Cal < 500</option>
      <option value="2">Cal >= 500</option>
    </select>
    <div *ngFor="#meal of meals | calorieFilter:filterState">
      <meal
        (click)="mealClicked(meal)"
        [class.selected]="meal === selectedMeal"
        [meal]="meal">
      </meal>
      <br>
    </div>
    <edit-meal *ngIf="selectedMeal" [selectedMeal]="selectedMeal"></edit-meal>
  `
})

export class MealListComponent {
  testMeal = new Meal("test", "good", 400);
  testMealTwo = new Meal("test2", "Better", 600);
  public meals: Meal[] = [this.testMeal, this.testMealTwo];
  public onMealSelect: EventEmitter<Meal>;
  public selectedMeal: Meal;
  editorHidden: boolean = true;
  public filterState: number = 0;

  constructor() {
    this.onMealSelect = new EventEmitter();
  }

  createMeal(meal: Meal): void {
    this.meals.push(meal);
  }

  mealClicked(clickedMeal: Meal): void {
    this.editorHidden = false;
    this.selectedMeal = clickedMeal;
    this.onMealSelect.emit(clickedMeal);
  }

  deselectMeal(deselectedMeal: Meal): void {
    console.log("deselect");
    this.editorHidden = true;
  }

  onChange(filterOption) {
    this.filterState = filterOption;
    console.log(this.filterState);
  }

}
