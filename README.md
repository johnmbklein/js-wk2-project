# _Food Tracker_

#### _An app that tracks caloric intake_

#### By _**John Klein*_

## Description

_Users can input caloric content of meals. App can filter meals by high or low caloric content. Built using written in TypeScript using JavaScript and Angular2_

## Setup/Installation Requirements for databases.

* To install:
* Clone Git repo
* cd into the project directory and run the following commands in Terminal:
  * npm install
  * bower install
  * npm install gulp -g
  * npm install bower -g
  * gem install sass
  * npm install typescript -g
  * apm install atom-typescript
  * gulp build
  * gulp serve


## Support and contact details

Email with any questions or comments.

## Technologies Used

_Bootstrap_
_HTML_
_JavaScript_
_Angular2_
_Node.js_
_Gulp_
_Bower_
_SASS/CSS_


### License

*This software is licensed under the GPL.  See license file for more information.*

Copyright (c) 2016 **_John Klein_**
